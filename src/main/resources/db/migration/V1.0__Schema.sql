/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ACHIEVEMENT` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `auto_update` bit(1) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `progress_text` varchar(255) DEFAULT NULL,
  `should_overwrite_progress` bit(1) DEFAULT b'0',
  `stat_conversion` enum('None','FromMetersToDistance','FromMillisecondsToMinutes') DEFAULT NULL,
  `update_trigger` text,
  `update_value` text,
  `visible` bit(1) DEFAULT NULL,
  `badge_definition_id` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ACHIEVEMENT_badge_definition_id` (`badge_definition_id`),
  KEY `ACHIEVEMENT_category_index` (`category`),
  CONSTRAINT `FK_ACHIEVEMENT_BADGE_DEFINITION_badge_definition_id` FOREIGN KEY (`badge_definition_id`) REFERENCES `BADGE_DEFINITION` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ACHIEVEMENT_RANK` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `points` int DEFAULT NULL,
  `rank` int DEFAULT NULL,
  `rarity` float DEFAULT NULL,
  `reward_description` varchar(255) DEFAULT NULL,
  `reward_type` varchar(255) DEFAULT NULL,
  `reward_visual_style` varchar(255) DEFAULT NULL,
  `threshold_value` int DEFAULT NULL,
  `achievement_id` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ACHIEVEMENT_RANK_ACHIEVEMENT_achievement_id` (`achievement_id`),
  CONSTRAINT `FK_ACHIEVEMENT_RANK_ACHIEVEMENT_achievement_id` FOREIGN KEY (`achievement_id`) REFERENCES `ACHIEVEMENT` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ACHIEVEMENT_REWARD` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `internal_reward_description` varchar(255) DEFAULT NULL,
  `reward_description` varchar(255) DEFAULT NULL,
  `rewardScript` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ACHIEVEMENT_REWARD_internal_reward_description_index` (`internal_reward_description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AMPLIFIERS` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `ampType` varchar(255) DEFAULT NULL,
  `cashMultiplier` float DEFAULT NULL,
  `repMultiplier` float DEFAULT NULL,
  `product_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_AMPLIFIERS_product_id` (`product_id`),
  CONSTRAINT `FK_AMPLIFIERS_PRODUCT_product_id` FOREIGN KEY (`product_id`) REFERENCES `PRODUCT` (`productId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BADGE_DEFINITION` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `background` varchar(255) DEFAULT NULL,
  `border` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BAN` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `data` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ends_at` datetime DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  `started` datetime DEFAULT NULL,
  `banned_by_id` bigint DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `BAN_endsAt_index` (`ends_at`),
  KEY `BAN_existence_index` (`user_id`,`ends_at`),
  KEY `FK_BAN_PERSONA_banned_by_id` (`banned_by_id`),
  CONSTRAINT `FK_BAN_PERSONA_banned_by_id` FOREIGN KEY (`banned_by_id`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_BAN_USER_user_id` FOREIGN KEY (`user_id`) REFERENCES `USER` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BASKETDEFINITION` (
  `productId` varchar(255) NOT NULL,
  `ownedCarTrans` longtext,
  PRIMARY KEY (`productId`),
  UNIQUE KEY `BASKETDEFINITION_productId_uindex` (`productId`),
  CONSTRAINT `FK_BASKETDEFINITION_PRODUCT_productId` FOREIGN KEY (`productId`) REFERENCES `PRODUCT` (`productId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CAR` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `durability` int NOT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `heat` float NOT NULL,
  `ownershipType` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `personaId` bigint NOT NULL,
  `baseCar` int NOT NULL,
  `carClassHash` int NOT NULL,
  `isPreset` bit(1) NOT NULL,
  `level` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `physicsProfileHash` int NOT NULL,
  `rating` int NOT NULL,
  `resalePrice` float NOT NULL,
  `rideHeightDrop` float NOT NULL,
  `skillModSlotCount` int NOT NULL,
  `version` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CAR_PERSONA_personaId` (`personaId`),
  KEY `IDX_CAR_expirationDate` (`expirationDate`),
  KEY `IDX_CAR_ownershipType` (`ownershipType`),
  CONSTRAINT `FK_CAR_PERSONA_personaId` FOREIGN KEY (`personaId`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CARD_PACK` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `entitlementTag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CARD_PACK_ITEM` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `script` text NOT NULL,
  `cardPackEntity_ID` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_CARD_PACK_ITEM_CARD_PACK_cardPackEntity_ID` (`cardPackEntity_ID`),
  CONSTRAINT `FK_CARD_PACK_ITEM_CARD_PACK_cardPackEntity_ID` FOREIGN KEY (`cardPackEntity_ID`) REFERENCES `CARD_PACK` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CAR_CLASSES` (
  `store_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `manufactor` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `ts_stock` int DEFAULT NULL,
  `ts_var1` int DEFAULT NULL,
  `ts_var2` int DEFAULT NULL,
  `ts_var3` int DEFAULT NULL,
  `ac_stock` int DEFAULT NULL,
  `ac_var1` int DEFAULT NULL,
  `ac_var2` int DEFAULT NULL,
  `ac_var3` int DEFAULT NULL,
  `ha_stock` int DEFAULT NULL,
  `ha_var1` int DEFAULT NULL,
  `ha_var2` int DEFAULT NULL,
  `ha_var3` int DEFAULT NULL,
  `hash` int DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`store_name`),
  UNIQUE KEY `store_name_index` (`store_name`),
  KEY `hash_index` (`hash`),
  KEY `store_name_key` (`store_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CATEGORY` (
  `idcategory` bigint NOT NULL AUTO_INCREMENT,
  `catalogVersion` varchar(255) DEFAULT NULL,
  `categories` varchar(255) DEFAULT NULL,
  `displayName` varchar(255) DEFAULT NULL,
  `filterType` int DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `id` bigint DEFAULT NULL,
  `longDescription` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `priority` smallint DEFAULT NULL,
  `shortDescription` varchar(255) DEFAULT NULL,
  `showInNavigationPane` bit(1) DEFAULT NULL,
  `showPromoPage` bit(1) DEFAULT NULL,
  `webIcon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idcategory`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CHAT_ANNOUNCEMENT` (
  `id` int NOT NULL AUTO_INCREMENT,
  `announcementInterval` int DEFAULT NULL,
  `announcementMessage` varchar(255) DEFAULT NULL,
  `channelMask` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CHAT_ROOM` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `amount` int DEFAULT NULL,
  `longName` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `shortName` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EVENT` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `eventModeId` int NOT NULL,
  `isEnabled` bit(1) DEFAULT b'1',
  `isLocked` bit(1) DEFAULT b'0',
  `rewardsTimeLimit` bigint NOT NULL DEFAULT '0',
  `maxCarClassRating` int NOT NULL,
  `maxLevel` int NOT NULL,
  `maxPlayers` int NOT NULL,
  `minCarClassRating` int NOT NULL,
  `minLevel` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carClassHash` int NOT NULL,
  `trackLength` float NOT NULL,
  `isRotationEnabled` bit(1) DEFAULT b'0',
  `dnfTimerTime` int DEFAULT '60000',
  `lobbyCountdownTime` int DEFAULT '60000',
  `legitTime` bigint DEFAULT '0',
  `isDnfEnabled` bit(1) DEFAULT b'1',
  `isRaceAgainEnabled` bit(1) DEFAULT b'1',
  `singleplayer_reward_config_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `multiplayer_reward_config_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `private_reward_config_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `EVENT_availability_index` (`isEnabled`,`minLevel`,`maxLevel` DESC),
  KEY `test_index` (`ID`,`name`),
  KEY `FK_EVENT_SINGLEPLAYER_REWARD_CONFIG_ID` (`singleplayer_reward_config_id`),
  KEY `FK_EVENT_MULTIPLAYER_REWARD_CONFIG_ID` (`multiplayer_reward_config_id`),
  KEY `FK_EVENT_PRIVATE_REWARD_CONFIG_ID` (`private_reward_config_id`),
  CONSTRAINT `FK_EVENT_MULTIPLAYER_REWARD_CONFIG_ID` FOREIGN KEY (`multiplayer_reward_config_id`) REFERENCES `EVENT_REWARD` (`ID`),
  CONSTRAINT `FK_EVENT_PRIVATE_REWARD_CONFIG_ID` FOREIGN KEY (`private_reward_config_id`) REFERENCES `EVENT_REWARD` (`ID`),
  CONSTRAINT `FK_EVENT_SINGLEPLAYER_REWARD_CONFIG_ID` FOREIGN KEY (`singleplayer_reward_config_id`) REFERENCES `EVENT_REWARD` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EVENT_DATA` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `alternateEventDurationInMilliseconds` bigint NOT NULL,
  `bestLapDurationInMilliseconds` bigint NOT NULL,
  `bustedCount` int NOT NULL,
  `carId` bigint NOT NULL,
  `copsDeployed` int NOT NULL,
  `copsDisabled` int NOT NULL,
  `copsRammed` int NOT NULL,
  `costToState` int NOT NULL,
  `distanceToFinish` float NOT NULL,
  `eventDurationInMilliseconds` bigint NOT NULL,
  `eventModeId` int NOT NULL,
  `eventSessionId` bigint DEFAULT NULL,
  `finishReason` int NOT NULL,
  `fractionCompleted` float NOT NULL,
  `hacksDetected` bigint NOT NULL,
  `heat` float NOT NULL,
  `infractions` int NOT NULL,
  `longestJumpDurationInMilliseconds` bigint NOT NULL,
  `numberOfCollisions` int NOT NULL,
  `perfectStart` int NOT NULL,
  `personaId` bigint DEFAULT NULL,
  `rank` int NOT NULL,
  `roadBlocksDodged` int NOT NULL,
  `spikeStripsDodged` int NOT NULL,
  `sumOfJumpsDurationInMilliseconds` bigint NOT NULL,
  `topSpeed` float NOT NULL,
  `EVENTID` int DEFAULT NULL,
  `isLegit` bit(1) DEFAULT b'0',
  `serverTimeInMilliseconds` bigint DEFAULT NULL,
  `serverTimeStarted` bigint DEFAULT NULL,
  `serverTimeEnded` bigint DEFAULT NULL,
  `carClassHash` int DEFAULT NULL,
  `carRating` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `esi_personaId` (`eventSessionId`,`personaId`),
  KEY `EVENT_DATA_persona_id_index` (`personaId`),
  KEY `car_id_index` (`carId`),
  KEY `finishreason_index` (`finishReason`),
  KEY `FK_EVENT_DATA_EVENT_EVENTID` (`EVENTID`),
  CONSTRAINT `FK_EVENT_DATA_EVENT_EVENTID` FOREIGN KEY (`EVENTID`) REFERENCES `EVENT` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_EVENT_DATA_EVENT_SESSION_eventSessionId` FOREIGN KEY (`eventSessionId`) REFERENCES `EVENT_SESSION` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_EVENT_DATA_PERSONA_personaId` FOREIGN KEY (`personaId`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EVENT_REWARD` (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `baseRepReward` int NOT NULL DEFAULT '0',
  `levelRepRewardMultiplier` float NOT NULL DEFAULT '0',
  `finalRepRewardMultiplier` float NOT NULL DEFAULT '0',
  `perfectStartRepMultiplier` float NOT NULL DEFAULT '0',
  `topSpeedRepMultiplier` float NOT NULL DEFAULT '0',
  `rank1RepMultiplier` float NOT NULL DEFAULT '0',
  `rank2RepMultiplier` float NOT NULL DEFAULT '0',
  `rank3RepMultiplier` float NOT NULL DEFAULT '0',
  `rank4RepMultiplier` float NOT NULL DEFAULT '0',
  `rank5RepMultiplier` float NOT NULL DEFAULT '0',
  `rank6RepMultiplier` float NOT NULL DEFAULT '0',
  `rank7RepMultiplier` float NOT NULL DEFAULT '0',
  `rank8RepMultiplier` float NOT NULL DEFAULT '0',
  `baseCashReward` int NOT NULL DEFAULT '0',
  `levelCashRewardMultiplier` float NOT NULL DEFAULT '0',
  `finalCashRewardMultiplier` float NOT NULL DEFAULT '0',
  `perfectStartCashMultiplier` float NOT NULL DEFAULT '0',
  `topSpeedCashMultiplier` float NOT NULL DEFAULT '0',
  `rank1CashMultiplier` float NOT NULL DEFAULT '0',
  `rank2CashMultiplier` float NOT NULL DEFAULT '0',
  `rank3CashMultiplier` float NOT NULL DEFAULT '0',
  `rank4CashMultiplier` float NOT NULL DEFAULT '0',
  `rank5CashMultiplier` float NOT NULL DEFAULT '0',
  `rank6CashMultiplier` float NOT NULL DEFAULT '0',
  `rank7CashMultiplier` float NOT NULL DEFAULT '0',
  `rank8CashMultiplier` float NOT NULL DEFAULT '0',
  `minTopSpeedTrigger` float NOT NULL DEFAULT '0',
  `rewardTable_rank1_id` bigint DEFAULT NULL,
  `rewardTable_rank2_id` bigint DEFAULT NULL,
  `rewardTable_rank3_id` bigint DEFAULT NULL,
  `rewardTable_rank4_id` bigint DEFAULT NULL,
  `rewardTable_rank5_id` bigint DEFAULT NULL,
  `rewardTable_rank6_id` bigint DEFAULT NULL,
  `rewardTable_rank7_id` bigint DEFAULT NULL,
  `rewardTable_rank8_id` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_EVENT_REWARD_RANK1TABLE_ID` (`rewardTable_rank1_id`),
  KEY `FK_EVENT_REWARD_RANK2TABLE_ID` (`rewardTable_rank2_id`),
  KEY `FK_EVENT_REWARD_RANK3TABLE_ID` (`rewardTable_rank3_id`),
  KEY `FK_EVENT_REWARD_RANK4TABLE_ID` (`rewardTable_rank4_id`),
  KEY `FK_EVENT_REWARD_RANK5TABLE_ID` (`rewardTable_rank5_id`),
  KEY `FK_EVENT_REWARD_RANK6TABLE_ID` (`rewardTable_rank6_id`),
  KEY `FK_EVENT_REWARD_RANK7TABLE_ID` (`rewardTable_rank7_id`),
  KEY `FK_EVENT_REWARD_RANK8TABLE_ID` (`rewardTable_rank8_id`),
  CONSTRAINT `FK_EVENT_REWARD_RANK1TABLE_ID` FOREIGN KEY (`rewardTable_rank1_id`) REFERENCES `REWARD_TABLE` (`ID`),
  CONSTRAINT `FK_EVENT_REWARD_RANK2TABLE_ID` FOREIGN KEY (`rewardTable_rank2_id`) REFERENCES `REWARD_TABLE` (`ID`),
  CONSTRAINT `FK_EVENT_REWARD_RANK3TABLE_ID` FOREIGN KEY (`rewardTable_rank3_id`) REFERENCES `REWARD_TABLE` (`ID`),
  CONSTRAINT `FK_EVENT_REWARD_RANK4TABLE_ID` FOREIGN KEY (`rewardTable_rank4_id`) REFERENCES `REWARD_TABLE` (`ID`),
  CONSTRAINT `FK_EVENT_REWARD_RANK5TABLE_ID` FOREIGN KEY (`rewardTable_rank5_id`) REFERENCES `REWARD_TABLE` (`ID`),
  CONSTRAINT `FK_EVENT_REWARD_RANK6TABLE_ID` FOREIGN KEY (`rewardTable_rank6_id`) REFERENCES `REWARD_TABLE` (`ID`),
  CONSTRAINT `FK_EVENT_REWARD_RANK7TABLE_ID` FOREIGN KEY (`rewardTable_rank7_id`) REFERENCES `REWARD_TABLE` (`ID`),
  CONSTRAINT `FK_EVENT_REWARD_RANK8TABLE_ID` FOREIGN KEY (`rewardTable_rank8_id`) REFERENCES `REWARD_TABLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EVENT_SESSION` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `EVENTID` int DEFAULT NULL,
  `ENDED` bigint DEFAULT NULL,
  `STARTED` bigint DEFAULT NULL,
  `LOBBYID` bigint DEFAULT NULL,
  `NEXTLOBBYID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_EVENT_SESSION_EVENT_EVENTID` (`EVENTID`),
  KEY `FK_EVENT_SESSION_LOBBY_LOBBYID` (`LOBBYID`),
  KEY `FK_EVENT_SESSION_LOBBY_NEXTLOBBYID` (`NEXTLOBBYID`),
  CONSTRAINT `FK_EVENT_SESSION_EVENT_EVENTID` FOREIGN KEY (`EVENTID`) REFERENCES `EVENT` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_EVENT_SESSION_LOBBY_LOBBYID` FOREIGN KEY (`LOBBYID`) REFERENCES `LOBBY` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_EVENT_SESSION_LOBBY_NEXTLOBBYID` FOREIGN KEY (`NEXTLOBBYID`) REFERENCES `LOBBY` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GIFT_CODE` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `endsAt` datetime DEFAULT NULL,
  `startsAt` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `HARDWARE_INFO` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `banned` bit(1) NOT NULL,
  `hardwareHash` varchar(255) DEFAULT NULL,
  `hardwareInfo` longtext,
  `userId` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `HARDWARE_INFO_hardwareHash_index` (`hardwareHash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `INVENTORY` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `performancePartsCapacity` int DEFAULT NULL,
  `performancePartsUsedSlotCount` int DEFAULT NULL,
  `skillModPartsCapacity` int DEFAULT NULL,
  `skillModPartsUsedSlotCount` int DEFAULT NULL,
  `visualPartsCapacity` int DEFAULT NULL,
  `visualPartsUsedSlotCount` int DEFAULT NULL,
  `personaId` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_INVENTORY_PERSONA_personaId` (`personaId`),
  CONSTRAINT `FK_INVENTORY_PERSONA_personaId` FOREIGN KEY (`personaId`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `INVENTORY_ITEM` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `expirationDate` datetime DEFAULT NULL,
  `remainingUseCount` int DEFAULT NULL,
  `resellPrice` int DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `inventoryEntity_id` bigint NOT NULL,
  `productId` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `INVENTORY_ITEM_expirationDate_index` (`expirationDate`),
  KEY `FK_INVENTORY_ITEM_PRODUCT_productId` (`productId`),
  KEY `FK_INVENTORY_ITEM_INVENTORY_inventoryEntity_id` (`inventoryEntity_id`),
  CONSTRAINT `FK_INVENTORY_ITEM_INVENTORY_inventoryEntity_id` FOREIGN KEY (`inventoryEntity_id`) REFERENCES `INVENTORY` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_INVENTORY_ITEM_PRODUCT_productId` FOREIGN KEY (`productId`) REFERENCES `PRODUCT` (`productId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `INVITE_TICKET` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `DISCORD_NAME` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `TICKET` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `USERID` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INVITE_TICKET_TICKET_index` (`TICKET`),
  KEY `FK_INVITE_TICKET_USER_USERID` (`USERID`),
  CONSTRAINT `FK_INVITE_TICKET_USER_USERID` FOREIGN KEY (`USERID`) REFERENCES `USER` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LEVEL_REP` (
  `level` bigint NOT NULL AUTO_INCREMENT,
  `expPoint` bigint DEFAULT NULL,
  PRIMARY KEY (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LOBBY` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `isPrivate` bit(1) DEFAULT NULL,
  `lobbyDateTimeStart` datetime DEFAULT NULL,
  `personaId` bigint DEFAULT NULL,
  `EVENTID` int DEFAULT NULL,
  `startedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LOBBY_startedTime_index` (`startedTime`),
  KEY `FK_LOBBY_EVENT_EVENTID` (`EVENTID`),
  CONSTRAINT `FK_LOBBY_EVENT_EVENTID` FOREIGN KEY (`EVENTID`) REFERENCES `EVENT` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LOBBY_ENTRANT` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `gridIndex` int NOT NULL,
  `LOBBYID` bigint DEFAULT NULL,
  `PERSONAID` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_LOBBY_ENTRANT_LOBBY_LOBBYID` (`LOBBYID`),
  KEY `FK_LOBBY_ENTRANT_PERSONA_PERSONAID` (`PERSONAID`),
  CONSTRAINT `FK_LOBBY_ENTRANT_LOBBY_LOBBYID` FOREIGN KEY (`LOBBYID`) REFERENCES `LOBBY` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_LOBBY_ENTRANT_PERSONA_PERSONAID` FOREIGN KEY (`PERSONAID`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LOGIN_ANNOUNCEMENT` (
  `id` int NOT NULL AUTO_INCREMENT,
  `imageUrl` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `target` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'NotApplicable',
  `language` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `NEWS_ARTICLE` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `filters` varchar(255) DEFAULT NULL,
  `iconType` int DEFAULT NULL,
  `longHALId` varchar(255) DEFAULT NULL,
  `parameters` varchar(1000) DEFAULT NULL,
  `shortHALId` varchar(255) DEFAULT NULL,
  `sticky` int DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(255) DEFAULT NULL,
  `persona_id` bigint DEFAULT NULL,
  `referenced_persona_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_NEWS_ARTICLE_PERSONA_referenced_persona_id` (`referenced_persona_id`),
  KEY `FK_NEWS_ARTICLE_PERSONA_persona_id` (`persona_id`),
  CONSTRAINT `FK_NEWS_ARTICLE_PERSONA_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_NEWS_ARTICLE_PERSONA_referenced_persona_id` FOREIGN KEY (`referenced_persona_id`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ONLINE_USERS` (
  `ID` int NOT NULL,
  `numberOfOnline` bigint NOT NULL,
  `numberOfRegistered` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ONLINE_USERS_id_index` (`ID` DESC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PAINT` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `paintGroup` int DEFAULT NULL,
  `hue` int NOT NULL,
  `sat` int NOT NULL,
  `slot` int NOT NULL,
  `paintVar` int DEFAULT NULL,
  `carId` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PAINT_CAR_carId` (`carId`),
  CONSTRAINT `FK_PAINT_CAR_carId` FOREIGN KEY (`carId`) REFERENCES `CAR` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PARAMETER` (
  `name` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `PARAMETER_name_index` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PERFORMANCEPART` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `performancePartAttribHash` int NOT NULL,
  `carId` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PERFORMANCEPART_CAR_carId` (`carId`),
  CONSTRAINT `FK_PERFORMANCEPART_CAR_carId` FOREIGN KEY (`carId`) REFERENCES `CAR` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PERSONA` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `boost` double NOT NULL,
  `cash` double NOT NULL,
  `curCarIndex` int NOT NULL,
  `iconIndex` int NOT NULL,
  `level` int NOT NULL,
  `motto` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `percentToLevel` float NOT NULL,
  `rating` double NOT NULL,
  `rep` double NOT NULL,
  `repAtCurrentLevel` int NOT NULL,
  `score` int NOT NULL,
  `USERID` bigint DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `badges` varchar(2048) DEFAULT NULL,
  `first_login` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PERSONA_name_index` (`name`),
  KEY `FK_PERSONA_USER_USERID` (`USERID`),
  CONSTRAINT `FK_PERSONA_USER_USERID` FOREIGN KEY (`USERID`) REFERENCES `USER` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PERSONA_ACHIEVEMENT` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `can_progress` bit(1) DEFAULT NULL,
  `current_value` bigint DEFAULT NULL,
  `achievement_id` bigint NOT NULL,
  `persona_id` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `persona_ach_index` (`persona_id`,`achievement_id`),
  KEY `FK_PERSONA_ACHIEVEMENT_ACHIEVEMENT_achievement_id` (`achievement_id`),
  CONSTRAINT `FK_PERSONA_ACHIEVEMENT_ACHIEVEMENT_achievement_id` FOREIGN KEY (`achievement_id`) REFERENCES `ACHIEVEMENT` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_PERSONA_ACHIEVEMENT_PERSONA_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PERSONA_ACHIEVEMENT_RANK` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `achieved_on` datetime DEFAULT NULL,
  `state` enum('Locked','InProgress','Completed','RewardWaiting') DEFAULT NULL,
  `achievement_rank_id` bigint NOT NULL,
  `persona_achievement_id` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_PERSONA_ACHIEVEMENT_RANK_PERSONA_ACHIEVEMENT_id` (`persona_achievement_id`),
  KEY `FK_PERSONA_ACHIEVEMENT_RANK_ACHIEVEMENT_RANK_id` (`achievement_rank_id`),
  CONSTRAINT `FK_PERSONA_ACHIEVEMENT_RANK_ACHIEVEMENT_RANK_id` FOREIGN KEY (`achievement_rank_id`) REFERENCES `ACHIEVEMENT_RANK` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_PERSONA_ACHIEVEMENT_RANK_PERSONA_ACHIEVEMENT_id` FOREIGN KEY (`persona_achievement_id`) REFERENCES `PERSONA_ACHIEVEMENT` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PERSONA_BADGE` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `slot` int DEFAULT NULL,
  `badge_definition_id` bigint NOT NULL,
  `persona_id` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_PERSONA_BADGE_BADGE_DEFINITION_badge_definition_id` (`badge_definition_id`),
  KEY `FK_PERSONA_BADGE_PERSONA_persona_id` (`persona_id`),
  CONSTRAINT `FK_PERSONA_BADGE_BADGE_DEFINITION_badge_definition_id` FOREIGN KEY (`badge_definition_id`) REFERENCES `BADGE_DEFINITION` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_PERSONA_BADGE_PERSONA_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PRODUCT` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `accel` int DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `categoryId` varchar(255) DEFAULT NULL,
  `categoryName` varchar(255) DEFAULT NULL,
  `currency` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `dropWeight` double DEFAULT NULL,
  `durationMinute` int NOT NULL,
  `enabled` bit(1) NOT NULL,
  `entitlementTag` varchar(255) DEFAULT NULL,
  `handling` int DEFAULT NULL,
  `hash` int DEFAULT NULL,
  `icon` varchar(255) NOT NULL,
  `isDropable` bit(1) NOT NULL,
  `level` int NOT NULL,
  `longDescription` varchar(255) DEFAULT NULL,
  `minLevel` int NOT NULL,
  `premium` bit(1) NOT NULL,
  `price` float NOT NULL,
  `priority` int NOT NULL,
  `productId` varchar(255) NOT NULL,
  `productTitle` varchar(255) DEFAULT NULL,
  `productType` varchar(255) NOT NULL,
  `rarity` int DEFAULT NULL,
  `resalePrice` float NOT NULL,
  `secondaryIcon` varchar(255) DEFAULT NULL,
  `skillValue` float DEFAULT NULL,
  `subType` varchar(255) DEFAULT NULL,
  `topSpeed` int DEFAULT NULL,
  `useCount` int NOT NULL,
  `visualStyle` varchar(255) DEFAULT NULL,
  `webIcon` varchar(255) DEFAULT NULL,
  `webLocation` varchar(255) DEFAULT NULL,
  `parentProductId` bigint DEFAULT NULL,
  `bundleItems` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_PRODUCT_productId` (`productId`),
  KEY `PRODUCT_availability_index` (`categoryName`,`productType`,`enabled`,`minLevel`,`premium`),
  KEY `PRODUCT_entitlementTag_index` (`entitlementTag`),
  KEY `PRODUCT_hash_index` (`hash`),
  KEY `parent_prod_id_index` (`parentProductId`),
  KEY `prod_id_index` (`productId`),
  CONSTRAINT `FK_PRODUCT_PRODUCT_parentProductId` FOREIGN KEY (`parentProductId`) REFERENCES `PRODUCT` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PROMO_CODE` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `isUsed` bit(1) DEFAULT NULL,
  `promoCode` varchar(255) DEFAULT NULL,
  `USERID` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PROMO_CODE_USER_USERID` (`USERID`),
  CONSTRAINT `FK_PROMO_CODE_USER_USERID` FOREIGN KEY (`USERID`) REFERENCES `USER` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RECOVERY_PASSWORD` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `expirationDate` datetime DEFAULT NULL,
  `isClose` bit(1) DEFAULT NULL,
  `randomKey` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `userId` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REPORT` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `abuserPersonaId` bigint DEFAULT NULL,
  `chatMinutes` int DEFAULT NULL,
  `customCarID` int DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hacksdetected` bigint DEFAULT NULL,
  `personaId` bigint DEFAULT NULL,
  `petitionType` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REWARD_TABLE` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `REWARD_TABLE_name_index` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REWARD_TABLE_ITEM` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `dropWeight` double DEFAULT NULL,
  `script` text NOT NULL,
  `rewardTableEntity_ID` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_REWARD_TABLE_ITEM_REWARD_TABLE_rewardTableEntity_ID` (`rewardTableEntity_ID`),
  CONSTRAINT `FK_REWARD_TABLE_ITEM_REWARD_TABLE_rewardTableEntity_ID` FOREIGN KEY (`rewardTableEntity_ID`) REFERENCES `REWARD_TABLE` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SERVER_INFO` (
  `messageSrv` varchar(1000) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `country` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `adminList` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `bannerUrl` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `discordUrl` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `facebookUrl` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `homePageUrl` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `numberOfRegistered` int DEFAULT NULL,
  `ownerList` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `serverName` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `timezone` int DEFAULT NULL,
  `activatedHolidaySceneryGroups` varchar(255) NOT NULL DEFAULT '',
  `disactivatedHolidaySceneryGroups` varchar(255) NOT NULL DEFAULT '',
  `allowedCountries` varchar(255) DEFAULT NULL,
  `secondsToShutDown` int DEFAULT '7200',
  PRIMARY KEY (`serverName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SKILLMODPART` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `isFixed` bit(1) NOT NULL,
  `skillModPartAttribHash` int NOT NULL,
  `carId` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SKILLMODPART_CAR_carId` (`carId`),
  CONSTRAINT `FK_SKILLMODPART_CAR_carId` FOREIGN KEY (`carId`) REFERENCES `CAR` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SOCIAL_RELATIONSHIP` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `remotePersonaId` bigint DEFAULT NULL,
  `status` bigint DEFAULT NULL,
  `fromUserId` bigint DEFAULT NULL,
  `userId` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_SOCIAL_RELATIONSHIP_USER_fromUserId` (`fromUserId`),
  KEY `FK_SOCIAL_RELATIONSHIP_USER_userId` (`userId`),
  KEY `FK_SOCIAL_RELATIONSHIP_PERSONA_remotePersonaId` (`remotePersonaId`),
  CONSTRAINT `FK_SOCIAL_RELATIONSHIP_PERSONA_remotePersonaId` FOREIGN KEY (`remotePersonaId`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_SOCIAL_RELATIONSHIP_USER_fromUserId` FOREIGN KEY (`fromUserId`) REFERENCES `USER` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_SOCIAL_RELATIONSHIP_USER_userId` FOREIGN KEY (`userId`) REFERENCES `USER` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TOKEN_SESSION` (
  `ID` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `activeLobbyId` bigint DEFAULT NULL,
  `activePersonaId` bigint DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `premium` bit(1) NOT NULL DEFAULT b'0',
  `relayCryptoTicket` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `userId` bigint DEFAULT NULL,
  `clientHostIp` varchar(255) DEFAULT NULL,
  `webToken` varchar(255) DEFAULT NULL,
  `eventSessionId` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TOKEN_SESSION_ID_uindex` (`ID`),
  UNIQUE KEY `UK_9ranmagnxgrp70u76q860goeb` (`userId`),
  KEY `TOKEN_SESSION_activePersonaId_index` (`activePersonaId`),
  KEY `event_session_fk` (`eventSessionId`),
  CONSTRAINT `FKomwojh6l6a26jsu4jiqpjnuvn` FOREIGN KEY (`userId`) REFERENCES `USER` (`ID`),
  CONSTRAINT `TOKEN_SESSION_ibfk_1` FOREIGN KEY (`eventSessionId`) REFERENCES `EVENT_SESSION` (`ID`) ON DELETE SET NULL,
  CONSTRAINT `TOKEN_SESSION_userID__fk` FOREIGN KEY (`userId`) REFERENCES `USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TREASURE_HUNT` (
  `personaId` bigint NOT NULL,
  `coinsCollected` int DEFAULT NULL,
  `isStreakBroken` bit(1) DEFAULT NULL,
  `numCoins` int DEFAULT NULL,
  `seed` int DEFAULT NULL,
  `streak` int DEFAULT NULL,
  `thDate` date DEFAULT NULL,
  `isCompleted` bit(1) NOT NULL,
  PRIMARY KEY (`personaId`),
  CONSTRAINT `FK_TREASURE_HUNT_PERSONA_personaId` FOREIGN KEY (`personaId`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TREASURE_HUNT_CONFIG` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `base_cash` float DEFAULT NULL,
  `base_rep` float DEFAULT NULL,
  `cash_multiplier` float DEFAULT NULL,
  `rep_multiplier` float DEFAULT NULL,
  `streak` int DEFAULT NULL,
  `reward_table_id` bigint DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_TREASURE_HUNT_CONFIG_REWARD_TABLE_reward_table_id` (`reward_table_id`),
  CONSTRAINT `FK_TREASURE_HUNT_CONFIG_REWARD_TABLE_reward_table_id` FOREIGN KEY (`reward_table_id`) REFERENCES `REWARD_TABLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USED_POWERUP` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `personaId` bigint NOT NULL,
  `eventSessionId` bigint DEFAULT NULL,
  `powerupHash` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hash_index` (`powerupHash`),
  KEY `FK_USED_POWERUP_EVENT_SESSION_eventSessionId` (`eventSessionId`),
  KEY `FK_USED_POWERUP_PERSONA_personaId` (`personaId`),
  CONSTRAINT `FK_USED_POWERUP_EVENT_SESSION_eventSessionId` FOREIGN KEY (`eventSessionId`) REFERENCES `EVENT_SESSION` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_USED_POWERUP_PERSONA_personaId` FOREIGN KEY (`personaId`) REFERENCES `PERSONA` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `PASSWORD` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `premium` bit(1) NOT NULL DEFAULT b'0',
  `isAdmin` bit(1) DEFAULT NULL,
  `HWID` varchar(255) DEFAULT NULL,
  `IP_ADDRESS` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `gameHardwareHash` varchar(255) DEFAULT NULL,
  `isLocked` bit(1) DEFAULT NULL,
  `selectedPersonaIndex` int DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USER_email_index` (`EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `VINYL` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `hash` int NOT NULL,
  `hue1` int NOT NULL,
  `hue2` int NOT NULL,
  `hue3` int NOT NULL,
  `hue4` int NOT NULL,
  `layer` int NOT NULL,
  `mir` bit(1) NOT NULL,
  `rot` int NOT NULL,
  `sat1` int NOT NULL,
  `sat2` int NOT NULL,
  `sat3` int NOT NULL,
  `sat4` int NOT NULL,
  `scalex` int NOT NULL,
  `scaley` int NOT NULL,
  `shear` int NOT NULL,
  `tranx` int NOT NULL,
  `trany` int NOT NULL,
  `var1` int NOT NULL,
  `var2` int NOT NULL,
  `var3` int NOT NULL,
  `var4` int NOT NULL,
  `carId` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_VINYL_CAR_carId` (`carId`),
  CONSTRAINT `FK_VINYL_CAR_carId` FOREIGN KEY (`carId`) REFERENCES `CAR` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `VINYLPRODUCT` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bundleItems` varchar(255) DEFAULT NULL,
  `categoryId` varchar(255) DEFAULT NULL,
  `categoryName` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `durationMinute` int NOT NULL,
  `enabled` bit(1) NOT NULL,
  `entitlementTag` varchar(255) DEFAULT NULL,
  `hash` int DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `level` int NOT NULL,
  `longDescription` varchar(255) DEFAULT NULL,
  `minLevel` int NOT NULL,
  `premium` bit(1) NOT NULL,
  `price` float NOT NULL,
  `priority` int NOT NULL,
  `productId` varchar(255) DEFAULT NULL,
  `productTitle` varchar(255) DEFAULT NULL,
  `productType` varchar(255) DEFAULT NULL,
  `secondaryIcon` varchar(255) DEFAULT NULL,
  `useCount` int NOT NULL,
  `visualStyle` varchar(255) DEFAULT NULL,
  `webIcon` varchar(255) DEFAULT NULL,
  `webLocation` varchar(255) DEFAULT NULL,
  `parentCategoryId` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_VINYLPRODUCT_CATEGORY` (`parentCategoryId`),
  CONSTRAINT `FK_VINYLPRODUCT_CATEGORY` FOREIGN KEY (`parentCategoryId`) REFERENCES `CATEGORY` (`idcategory`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `VIRTUALITEM` (
  `itemName` varchar(255) NOT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `hash` int DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `longdescription` varchar(255) DEFAULT NULL,
  `rarity` int DEFAULT NULL,
  `resellprice` int DEFAULT NULL,
  `shortdescription` varchar(255) DEFAULT NULL,
  `subType` varchar(255) DEFAULT NULL,
  `tier` int DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `warnondelete` bit(1) DEFAULT NULL,
  PRIMARY KEY (`itemName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `VISUALPART` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `partHash` int NOT NULL,
  `slotHash` int NOT NULL,
  `carId` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_VISUALPART_CAR_carId` (`carId`),
  CONSTRAINT `FK_VISUALPART_CAR_carId` FOREIGN KEY (`carId`) REFERENCES `CAR` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

