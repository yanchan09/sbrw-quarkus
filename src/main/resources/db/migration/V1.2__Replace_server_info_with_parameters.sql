DROP TABLE `SERVER_INFO`;
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_ADMINS', '');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_BANNER_URL', '');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_COUNTRY', '');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_DISABLED_SCENERY', 'SCENERY_GROUP_NORMAL_DISABLE');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_DISCORD_URL', '');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_ENABLED_SCENERY', 'SCENERY_GROUP_NORMAL');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_FACEBOOK_URL', '');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_HOMEPAGE_URL', '');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_MESSAGE', '');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_NAME', 'My SBRW Server');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_OWNERS', '');
INSERT INTO `PARAMETER` (`name`, `value`) VALUES ('SERVER_INFO_TIMEZONE', '0');
