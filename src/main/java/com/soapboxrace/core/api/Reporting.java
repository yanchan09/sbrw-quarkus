/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import com.soapboxrace.core.api.util.Secured;
import com.soapboxrace.core.bo.HardwareInfoBO;
import com.soapboxrace.core.bo.RequestSessionInfo;
import com.soapboxrace.core.bo.TokenSessionBO;
import com.soapboxrace.core.dao.UserDAO;
import com.soapboxrace.core.jpa.HardwareInfoEntity;
import com.soapboxrace.core.jpa.UserEntity;
import com.soapboxrace.jaxb.http.HardwareInfo;
import com.soapboxrace.jaxb.util.JAXBUtility;
import io.smallrye.mutiny.Uni;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;

@Path("/Reporting")
public class Reporting {

    @Inject
    HardwareInfoBO hardwareInfoBO;

    @Inject
    TokenSessionBO tokenBO;

    @Inject
    UserDAO userDAO;

    @Inject
    RequestSessionInfo requestSessionInfo;

    @POST
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/SendHardwareInfo")
    public String sendHardwareInfo(InputStream is) {
        HardwareInfo hardwareInfo = JAXBUtility.unMarshal(is, HardwareInfo.class);
        if (hardwareInfo.getCpuid0().equals("GenuineIntel") || hardwareInfo.getCpuid0().equals("AuthenticAMD")) {
            HardwareInfoEntity hardwareInfoEntity = hardwareInfoBO.save(hardwareInfo);
            UserEntity user = requestSessionInfo.getUser();
            user.setGameHardwareHash(hardwareInfoEntity.getHardwareHash());
            userDAO.update(user);
        } else {
            tokenBO.deleteByUserId(requestSessionInfo.getUser().getId());
        }
        return "";
    }

    @POST
    @Produces(MediaType.APPLICATION_XML)
    @Path("/SendUserSettings")
    public Uni<String> sendUserSettings() {
        return Uni.createFrom().item("");
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("/SendMultiplayerConnect")
    public Uni<String> sendMultiplayerConnect() {
        return Uni.createFrom().item("");
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("/SendClientPingTime")
    public Uni<String> sendClientPingTime() {
        return Uni.createFrom().item("");
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("/LoginAnnouncementClicked")
    public Uni<String> loginAnnouncementClicked() {
        return Uni.createFrom().item("");
    }

    @PUT
    @Produces(MediaType.APPLICATION_XML)
    @Path("{path:.*}")
    public Uni<String> genericEmptyPut(@PathParam("path") String path) {
        return Uni.createFrom().item("");
    }
}
