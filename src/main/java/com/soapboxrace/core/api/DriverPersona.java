/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import com.soapboxrace.core.api.util.Secured;
import com.soapboxrace.core.bo.*;
import com.soapboxrace.core.engine.EngineException;
import com.soapboxrace.core.engine.EngineExceptionCode;
import com.soapboxrace.core.jpa.PersonaEntity;
import com.soapboxrace.jaxb.http.*;
import com.soapboxrace.jaxb.http.arrays.ArrayOfLong;
import com.soapboxrace.jaxb.http.driverpersona.PersonaIdArray;
import com.soapboxrace.jaxb.http.driverpersona.PersonaMotto;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.regex.Pattern;

@Path("/DriverPersona")
public class DriverPersona {

    private final Pattern NAME_PATTERN = Pattern.compile("^[A-Z0-9]{3,15}$");

    @Inject
    DriverPersonaBO driverPersonaBO;

    @Inject
    UserBO userBo;

    @Inject
    TokenSessionBO tokenSessionBo;

    @Inject
    PresenceBO presenceBO;

    @Inject
    RequestSessionInfo requestSessionInfo;

    @GET
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/GetExpLevelPointsMap")
    public ArrayOfInt getExpLevelPointsMap() {
        return driverPersonaBO.getExpLevelPointsMap();
    }

    @GET
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/GetPersonaInfo")
    public ProfileData getPersonaInfo(@QueryParam("personaId") Long personaId) {
        return driverPersonaBO.getPersonaInfo(personaId);
    }

    @POST
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/ReserveName")
    public ArrayOfString reserveName(@QueryParam("name") String name) {
        return driverPersonaBO.reserveName(name);
    }

    @POST
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/UnreserveName")
    public String UnreserveName(@QueryParam("name") String name) {
        return "";
    }

    @POST
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/CreatePersona")
    public ProfileData createPersona(@HeaderParam("userId") Long userId,
                                     @HeaderParam("securityToken") String securityToken,
                                     @QueryParam("name") String name,
                                     @QueryParam("iconIndex") int iconIndex, @QueryParam("clan") String clan,
                                     @QueryParam("clanIcon") String clanIcon) {
        if (!NAME_PATTERN.matcher(name).matches()) {
            throw new EngineException(EngineExceptionCode.DisplayNameNotAllowed, true);
        }

        ArrayOfString nameReserveResult = driverPersonaBO.reserveName(name);

        if (!nameReserveResult.getString().isEmpty()) {
            throw new EngineException(EngineExceptionCode.DisplayNameDuplicate, true);
        }

        PersonaEntity personaEntity = new PersonaEntity();
        personaEntity.setName(name);
        personaEntity.setIconIndex(iconIndex);
        ProfileData persona = driverPersonaBO.createPersona(userId, personaEntity);

        if (persona == null) {
            throw new EngineException(EngineExceptionCode.MaximumNumberOfPersonasForUserReached, true);
        }

        long personaId = persona.getPersonaId();
        requestSessionInfo.getTokenSessionEntity().getAllowedPersonaIds().add(personaId);
        userBo.createXmppUser(personaId, securityToken.substring(0, 16));
        return persona;
    }

    @POST
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/DeletePersona")
    public String deletePersona(@QueryParam("personaId") Long personaId) {
        tokenSessionBo.verifyPersonaOwnership(requestSessionInfo.getTokenSessionEntity(), personaId);
        driverPersonaBO.deletePersona(personaId);
        requestSessionInfo.getTokenSessionEntity().getAllowedPersonaIds().remove(personaId);
        return "<long>0</long>";
    }

    @POST
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/GetPersonaBaseFromList")
    public ArrayOfPersonaBase getPersonaBaseFromList(PersonaIdArray personaIdArray) {
        ArrayOfLong personaIds = personaIdArray.getPersonaIds();
        return driverPersonaBO.getPersonaBaseFromList(personaIds.getLong());
    }

    @POST
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/UpdatePersonaPresence")
    public String updatePersonaPresence(@QueryParam("personaId") Long personaId,
                                        @QueryParam("presence") Long presence) {
        tokenSessionBo.verifyPersonaOwnership(requestSessionInfo.getTokenSessionEntity(), personaId);
        presenceBO.updatePresence(personaId, presence);

        return "";
    }

    @GET
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/GetPersonaPresenceByName")
    public PersonaPresence getPersonaPresenceByName(@QueryParam("displayName") String displayName) {
        return driverPersonaBO.getPersonaPresenceByName(displayName);
    }

    @POST
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/UpdateStatusMessage")
    public PersonaMotto updateStatusMessage(PersonaMotto personaMotto) {
        tokenSessionBo.verifyPersonaOwnership(requestSessionInfo.getTokenSessionEntity(), personaMotto.getPersonaId());

        driverPersonaBO.updateStatusMessage(personaMotto.getMessage(), personaMotto.getPersonaId());
        return personaMotto;
    }
}