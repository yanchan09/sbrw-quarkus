/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import com.soapboxrace.core.api.util.Secured;
import com.soapboxrace.core.bo.RequestSessionInfo;
import com.soapboxrace.core.bo.SocialRelationshipBO;
import com.soapboxrace.jaxb.http.arrays.ArrayOfLong;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/getblockersbyusers")
public class GetBlockersByUsers {

    @Inject
    SocialRelationshipBO socialRelationshipBO;

    @Inject
    RequestSessionInfo requestSessionInfo;

    @GET
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    public ArrayOfLong getBlockedUserList(@HeaderParam("userId") Long userId,
                                          @HeaderParam("securityToken") String securityToken) {
        return socialRelationshipBO.getBlockersByUsers(requestSessionInfo.getActivePersonaId());
    }
}
