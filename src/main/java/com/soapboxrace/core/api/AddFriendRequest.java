/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import com.soapboxrace.core.api.util.Secured;
import com.soapboxrace.core.bo.RequestSessionInfo;
import com.soapboxrace.core.bo.SocialRelationshipBO;
import com.soapboxrace.jaxb.http.FriendResult;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/addfriendrequest")
public class AddFriendRequest {

    @Inject
    SocialRelationshipBO socialRelationshipBO;

    @Inject
    RequestSessionInfo requestSessionInfo;

    @GET
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    public FriendResult addFriendRequest(@HeaderParam("securityToken") String securityToken,
                                         @QueryParam("displayName") String displayName) {
        return socialRelationshipBO.addFriend(
                requestSessionInfo.getActivePersonaId(), displayName);
    }
}