/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import com.soapboxrace.core.api.util.Secured;
import com.soapboxrace.core.bo.SocialRelationshipBO;
import com.soapboxrace.core.bo.TokenSessionBO;
import com.soapboxrace.jaxb.http.PersonaBase;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/unblockplayer")
public class UnblockPlayer {
    @Inject
    SocialRelationshipBO socialRelationshipBO;

    @GET
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    public PersonaBase unblockPlayer(@HeaderParam("userId") Long userId,
                                     @HeaderParam("securityToken") String securityToken,
                                     @QueryParam("otherPersonaId") Long otherPersonaId) {
        return socialRelationshipBO.unblockPlayer(userId, otherPersonaId);
    }
}
