/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/")
public class Generic {

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("{path:.*}")
    public String genericEmptyGet(@PathParam("path") String path) {
        return "";
    }

    @POST
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("{path:.*}")
    public String genericEmptyPost(@PathParam("path") String path) {
        return "";
    }

    @PUT
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("{path:.*}")
    public String genericEmptyPut(@PathParam("path") String path) {
        return "";
    }

}
