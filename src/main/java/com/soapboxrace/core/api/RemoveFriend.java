/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import com.soapboxrace.core.api.util.Secured;
import com.soapboxrace.core.bo.RequestSessionInfo;
import com.soapboxrace.core.bo.SocialRelationshipBO;
import com.soapboxrace.jaxb.http.PersonaBase;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/removefriend")
public class RemoveFriend {

    @Inject
    SocialRelationshipBO socialRelationshipBO;

    @Inject
    RequestSessionInfo requestSessionInfo;

    @GET
    @Secured
    public PersonaBase removefriend(@QueryParam("friendPersonaId") Long friendPersonaId) {
        return socialRelationshipBO.removeFriend(requestSessionInfo.getActivePersonaId(),
                friendPersonaId);
    }
}