/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import com.soapboxrace.core.bo.ModdingBO;
import com.soapboxrace.core.vo.ModInfoVO;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/Modding")
public class Modding {

    @Inject
    ModdingBO moddingBO;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("GetModInfo")
    public ModInfoVO getModInfo() {
        ModInfoVO modInfoVO = moddingBO.getModInfo();

        if (modInfoVO == null) {
            throw new NotFoundException();
        }

        return modInfoVO;
    }
}
