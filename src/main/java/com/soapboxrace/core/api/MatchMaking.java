/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.api;

import com.soapboxrace.core.api.util.Secured;
import com.soapboxrace.core.bo.*;
import com.soapboxrace.core.jpa.EventSessionEntity;
import com.soapboxrace.core.jpa.TokenSessionEntity;
import com.soapboxrace.jaxb.http.LobbyInfo;
import com.soapboxrace.jaxb.http.OwnedCarTrans;
import com.soapboxrace.jaxb.http.SecurityChallenge;
import com.soapboxrace.jaxb.http.SessionInfo;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/matchmaking")
public class MatchMaking {

    @Inject
    EventBO eventBO;

    @Inject
    LobbyBO lobbyBO;

    @Inject
    TokenSessionBO tokenSessionBO;

    @Inject
    PersonaBO personaBO;

    @Inject
    MatchmakingBO matchmakingBO;

    @Inject
    RequestSessionInfo requestSessionInfo;

    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/joinqueueracenow")
    public String joinQueueRaceNow() {
        Long activePersonaId = requestSessionInfo.getActivePersonaId();
        OwnedCarTrans defaultCar = personaBO.getDefaultCar(activePersonaId);
        lobbyBO.joinFastLobby(activePersonaId, defaultCar.getCustomCar().getCarClassHash());
        return "";
    }

    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/joinqueueevent/{eventId}")
    public String joinQueueEvent(@PathParam("eventId") int eventId) {
        lobbyBO.joinQueueEvent(requestSessionInfo.getActivePersonaId(), eventId);
        return "";
    }

    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/leavequeue")
    public String leaveQueue() {
        matchmakingBO.removePlayerFromQueue(requestSessionInfo.getActivePersonaId());
        return "";
    }

    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/leavelobby")
    public String leavelobby() {
        Long activePersonaId = requestSessionInfo.getActivePersonaId();
        Long activeLobbyId = requestSessionInfo.getActiveLobbyId();
        if (activeLobbyId != null && !activeLobbyId.equals(0L)) {
            lobbyBO.removeEntrantFromLobby(activePersonaId, activeLobbyId);
        }
        return "";
    }

    @GET
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/launchevent/{eventId}")
    public SessionInfo launchEvent(@PathParam("eventId") int eventId) {
        TokenSessionEntity tokenSessionEntity = requestSessionInfo.getTokenSessionEntity();
        EventSessionEntity createEventSession = eventBO.createEventSession(tokenSessionEntity, eventId);

        SessionInfo sessionInfo = new SessionInfo();
        SecurityChallenge securityChallenge = new SecurityChallenge();
        securityChallenge.setChallengeId("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        securityChallenge.setLeftSize(14);
        securityChallenge.setPattern("FFFFFFFFFFFFFFFF");
        securityChallenge.setRightSize(50);
        sessionInfo.setChallenge(securityChallenge);
        sessionInfo.setEventId(eventId);
        sessionInfo.setSessionId(createEventSession.getId());
        tokenSessionBO.setActiveLobbyId(tokenSessionEntity, 0L);
        return sessionInfo;
    }

    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/makeprivatelobby/{eventId}")
    public String makePrivateLobby(@PathParam("eventId") int eventId) {
        lobbyBO.createPrivateLobby(requestSessionInfo.getActivePersonaId(), eventId);
        return "";
    }

    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/acceptinvite")
    public LobbyInfo acceptInvite(@QueryParam("lobbyInviteId") Long lobbyInviteId) {
        tokenSessionBO.setActiveLobbyId(requestSessionInfo.getTokenSessionEntity(), lobbyInviteId);
        return lobbyBO.acceptinvite(requestSessionInfo.getActivePersonaId(), lobbyInviteId);
    }

    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    @Transactional
    @Path("/declineinvite")
    public String declineInvite(@QueryParam("lobbyInviteId") Long lobbyInviteId) {
        lobbyBO.declineinvite(requestSessionInfo.getActivePersonaId(), lobbyInviteId);
        return "";
    }

}
