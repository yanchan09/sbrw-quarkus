/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.bo;

import com.soapboxrace.core.dao.OnlineUsersDAO;
import com.soapboxrace.core.dao.UserDAO;
import com.soapboxrace.core.jpa.OnlineUsersEntity;
import com.soapboxrace.core.xmpp.OpenFireRestApiCli;
import io.quarkus.arc.Lock;
import io.quarkus.logging.Log;
import io.quarkus.scheduler.Scheduled;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.util.Date;

@ApplicationScoped
@Lock
public class OnlineUsersBO {

    @Inject
    OpenFireRestApiCli openFireRestApiCli;

    @Inject
    OnlineUsersDAO onlineUsersDAO;

    @Inject
    UserDAO userDAO;

    private OnlineUsersEntity lastRecordedStats;

    public OnlineUsersEntity getOnlineUsersStats() {
        return lastRecordedStats;
    }

    @PostConstruct
    @Transactional
    void loadLastValue() {
        lastRecordedStats = onlineUsersDAO.findLast();

        if (lastRecordedStats == null) {
            OnlineUsersEntity fallback = new OnlineUsersEntity();
            fallback.setTimeRecord((int) (new Date().getTime() / 1000L));
            lastRecordedStats = fallback;
        }
    }

    @Scheduled(cron = "0 * * * * ?")
    @Transactional
    public void insertOnlineStats() {
        long timeLong = new Date().getTime() / 1000L;
        OnlineUsersEntity onlineUsersEntity = new OnlineUsersEntity();
        onlineUsersEntity.setNumberOfOnline(openFireRestApiCli.getTotalOnlineUsers());
        onlineUsersEntity.setNumberOfRegistered(userDAO.countUsers());
        onlineUsersEntity.setTimeRecord((int) timeLong);
        try {
            onlineUsersDAO.insert(onlineUsersEntity);
        } catch (PersistenceException ex) {
            Log.warnf("Failed to insert online stats for timestamp %d", timeLong);
        }
        lastRecordedStats = onlineUsersEntity;
    }
}
