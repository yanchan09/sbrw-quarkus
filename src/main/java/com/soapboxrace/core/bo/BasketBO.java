/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.bo;

import com.soapboxrace.core.bo.util.AchievementCommerceContext;
import com.soapboxrace.core.bo.util.OwnedCarConverter;
import com.soapboxrace.core.dao.*;
import com.soapboxrace.core.engine.EngineException;
import com.soapboxrace.core.engine.EngineExceptionCode;
import com.soapboxrace.core.events.CarAddedToDriver;
import com.soapboxrace.core.jpa.*;
import com.soapboxrace.jaxb.http.ArrayOfOwnedCarTrans;
import com.soapboxrace.jaxb.http.CommerceResultStatus;
import com.soapboxrace.jaxb.http.CommerceResultTrans;
import com.soapboxrace.jaxb.http.OwnedCarTrans;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.Map;
import java.util.Objects;

@Dependent
public class BasketBO {

    @Inject
    PersonaBO personaBo;

    @Inject
    ParameterBO parameterBO;

    @Inject
    CarDAO carDAO;

    @Inject
    CardPackDAO cardPackDAO;

    @Inject
    ProductDAO productDao;

    @Inject
    PersonaDAO personaDao;

    @Inject
    InventoryDAO inventoryDao;

    @Inject
    InventoryItemDAO inventoryItemDao;

    @Inject
    InventoryBO inventoryBO;

    @Inject
    TreasureHuntDAO treasureHuntDAO;

    @Inject
    AchievementBO achievementBO;

    @Inject
    CarDamageBO carDamageBO;

    @Inject
    CarSlotBO carSlotBO;

    @Inject
    AmplifierDAO amplifierDAO;

    @Inject
    ItemRewardBO itemRewardBO;

    public ProductEntity findProduct(String productId) {
        return productDao.findByProductId(productId);
    }

    public CommerceResultStatus repairCar(String productId, PersonaEntity personaEntity) {
        CarEntity defaultCarEntity = personaBo.getDefaultCarEntity(personaEntity.getPersonaId());
        int price =
                (int) (productDao.findByProductId(productId).getPrice() * (100 - defaultCarEntity.getDurability()));
        ProductEntity repairProduct = productDao.findByProductId(productId);

        if (repairProduct == null) {
            return CommerceResultStatus.FAIL_INVALID_BASKET;
        }
        if (this.canPurchaseProduct(personaEntity, repairProduct, price)) {
            personaDao.update(personaEntity);
            carDamageBO.updateDurability(defaultCarEntity, 100);
            carDAO.update(defaultCarEntity);
            this.performPersonaTransaction(personaEntity, repairProduct, price);
            return CommerceResultStatus.SUCCESS;
        }

        return CommerceResultStatus.FAIL_INSUFFICIENT_FUNDS;
    }

    public CommerceResultStatus buyPowerups(String productId, PersonaEntity personaEntity) {
        if (!parameterBO.getBoolParam("ENABLE_ECONOMY")) {
            return CommerceResultStatus.FAIL_INSUFFICIENT_FUNDS;
        }
        ProductEntity powerupProduct = productDao.findByProductId(productId);

        if (powerupProduct == null) {
            return CommerceResultStatus.FAIL_INVALID_BASKET;
        }

        if (canPurchaseProduct(personaEntity, powerupProduct)) {
            InventoryEntity inventoryEntity = inventoryDao.findByPersonaId(personaEntity.getPersonaId());
            inventoryBO.addStackedInventoryItem(inventoryEntity, productId, powerupProduct.getUseCount());
            inventoryDao.update(inventoryEntity);
            performPersonaTransaction(personaEntity, powerupProduct);
            return CommerceResultStatus.SUCCESS;
        }

        return CommerceResultStatus.FAIL_INSUFFICIENT_FUNDS;
    }

    public CommerceResultStatus buyCar(ProductEntity productEntity, PersonaEntity personaEntity, TokenSessionEntity tokenSessionEntity,
                                       CommerceResultTrans commerceResultTrans) {
        if (getPersonaCarCount(personaEntity.getPersonaId()) >= parameterBO.getCarLimit(tokenSessionEntity.getUserEntity())) {
            return CommerceResultStatus.FAIL_INSUFFICIENT_CAR_SLOTS;
        }

        if (canPurchaseProduct(personaEntity, productEntity)) {
            try {
                CarEntity carEntity = carSlotBO.addCar(productEntity, personaEntity);
                personaEntity.setCurCarIndex(carDAO.findNumByPersonaId(personaEntity.getPersonaId()) - 1);
                personaDao.update(personaEntity);

                ArrayOfOwnedCarTrans arrayOfOwnedCarTrans = new ArrayOfOwnedCarTrans();
                OwnedCarTrans ownedCarTrans = OwnedCarConverter.entity2Trans(carEntity);
                commerceResultTrans.setPurchasedCars(arrayOfOwnedCarTrans);
                arrayOfOwnedCarTrans.getOwnedCarTrans().add(ownedCarTrans);

                performPersonaTransaction(personaEntity, productEntity);
            } catch (EngineException e) {
                return CommerceResultStatus.FAIL_MAX_STACK_OR_RENTAL_LIMIT;
            }

            return CommerceResultStatus.SUCCESS;
        }

        return CommerceResultStatus.FAIL_INSUFFICIENT_FUNDS;
    }

    public CommerceResultStatus buyBundle(String productId, PersonaEntity personaEntity, CommerceResultTrans commerceResultTrans) {
        ProductEntity bundleProduct = productDao.findByProductId(productId);

        if (bundleProduct == null) {
            return CommerceResultStatus.FAIL_INVALID_BASKET;
        }

        if (canPurchaseProduct(personaEntity, bundleProduct)) {
            try {
                CardPackEntity cardPackEntity = cardPackDAO.findByEntitlementTag(bundleProduct.getEntitlementTag());

                if (cardPackEntity == null) {
                    throw new EngineException("Could not find card pack with name: " + bundleProduct.getEntitlementTag() + " (product ID: " + productId + ")", EngineExceptionCode.LuckyDrawCouldNotDrawProduct, true);
                }

                for (CardPackItemEntity cardPackItemEntity : cardPackEntity.getItems()) {
                    itemRewardBO.convertRewards(
                            itemRewardBO.getRewards(personaEntity, cardPackItemEntity.getScript()),
                            commerceResultTrans
                    );
                }

                performPersonaTransaction(personaEntity, bundleProduct);

                AchievementCommerceContext commerceContext = new AchievementCommerceContext(AchievementCommerceContext.CommerceType.BUNDLE_PURCHASE);
                commerceContext.setProductEntity(bundleProduct);
                AchievementTransaction achievementTransaction = achievementBO.createTransaction(personaEntity.getPersonaId());
                achievementTransaction.add("COMMERCE", Map.of("persona", personaEntity, "commerceCtx", commerceContext));
                achievementBO.commitTransaction(personaEntity, achievementTransaction);

                return CommerceResultStatus.SUCCESS;
            } catch (EngineException e) {
                throw new EngineException("Error occurred in bundle purchase (product ID: " + productId + ")", e, e.getCode(), true);
            }
        }

        return CommerceResultStatus.FAIL_INSUFFICIENT_FUNDS;
    }

    public CommerceResultStatus reviveTreasureHunt(String productId, PersonaEntity personaEntity) {
        ProductEntity productEntity = productDao.findByProductId(productId);

        if (canPurchaseProduct(personaEntity, productEntity)) {
            TreasureHuntEntity treasureHuntEntity = treasureHuntDAO.find(personaEntity.getPersonaId());
            treasureHuntEntity.setIsStreakBroken(false);
            performPersonaTransaction(personaEntity, productEntity);

            treasureHuntDAO.update(treasureHuntEntity);

            return CommerceResultStatus.SUCCESS;
        }

        return CommerceResultStatus.FAIL_INSUFFICIENT_FUNDS;
    }

    public CommerceResultStatus buyAmplifier(PersonaEntity personaEntity, String productId) {
        ProductEntity productEntity = productDao.findByProductId(productId);

        if (!canAddAmplifier(personaEntity.getPersonaId(), productEntity.getEntitlementTag())) {
            return CommerceResultStatus.FAIL_MAX_ALLOWED_PURCHASES_FOR_THIS_PRODUCT;
        }

        if (canPurchaseProduct(personaEntity, productEntity)) {
            addAmplifier(personaEntity, productEntity);
            performPersonaTransaction(personaEntity, productEntity);
            return CommerceResultStatus.SUCCESS;
        }

        return CommerceResultStatus.FAIL_INSUFFICIENT_FUNDS;
    }

    void onCarAddedToDriver(@Observes CarAddedToDriver event) {
        boolean isRental = Objects.equals(event.getCarEntity().getOwnershipType(), "RentalCar");
        if (isRental && canAddAmplifier(event.getPersonaEntity().getPersonaId(), "INSURANCE_AMPLIFIER")) {
            addAmplifier(event.getPersonaEntity(), productDao.findByEntitlementTag("INSURANCE_AMPLIFIER"));
        }
    }

    private boolean canAddAmplifier(Long personaId, String entitlementTag) {
        return inventoryItemDao.findAllByPersonaIdAndEntitlementTag(personaId, entitlementTag).isEmpty();
    }

    private void addAmplifier(PersonaEntity personaEntity, ProductEntity productEntity) {
        InventoryEntity inventoryEntity = inventoryBO.getInventory(personaEntity);
        inventoryBO.addInventoryItem(inventoryEntity, productEntity.getProductId());

        AmplifierEntity amplifierEntity = amplifierDAO.findAmplifierByHash(productEntity.getHash());

        if (amplifierEntity.getAmpType().equals("INSURANCE")) {
            personaBo.repairAllCars(personaEntity);
        }
    }

    private int getPersonaCarCount(Long personaId) {
        return carSlotBO.countPersonasCar(personaId);
    }

    private boolean canPurchaseProduct(PersonaEntity personaEntity, ProductEntity productEntity) {
        return canPurchaseProduct(personaEntity, productEntity, -1);
    }

    private boolean canPurchaseProduct(PersonaEntity personaEntity, ProductEntity productEntity, float valueOverride) {
        if (productEntity.isEnabled()) {
            // non-premium products are available to all; user must be premium to purchase a premium product
            if (productEntity.isPremium() && !personaEntity.getUser().isPremium()) {
                return false;
            }

            float price = valueOverride == -1 ? productEntity.getPrice() : valueOverride;

            switch (productEntity.getCurrency()) {
                case "CASH":
                    return personaEntity.getCash() >= price;
                case "_NS":
                    return personaEntity.getBoost() >= price;
                default:
                    throw new EngineException("Invalid currency in product entry: " + productEntity.getCurrency(), EngineExceptionCode.UnspecifiedError, true);
            }
        }

        return false;
    }

    private void performPersonaTransaction(PersonaEntity personaEntity, ProductEntity productEntity) {
        performPersonaTransaction(personaEntity, productEntity, -1);
    }

    private void performPersonaTransaction(PersonaEntity personaEntity, ProductEntity productEntity, float valueOverride) {
        float price = valueOverride == -1 ? productEntity.getPrice() : valueOverride;

        switch (productEntity.getCurrency()) {
            case "CASH":
                personaEntity.setCash(personaEntity.getCash() - price);
                break;
            case "_NS":
                personaEntity.setBoost(personaEntity.getBoost() - price);
                break;
            default:
                throw new EngineException("Invalid currency in product entry: " + productEntity.getCurrency(), EngineExceptionCode.UnspecifiedError, true);
        }

        personaDao.update(personaEntity);
    }
}
