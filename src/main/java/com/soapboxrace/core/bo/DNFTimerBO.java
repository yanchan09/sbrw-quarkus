package com.soapboxrace.core.bo;

import com.soapboxrace.core.jpa.EventSessionEntity;
import com.soapboxrace.core.xmpp.OpenFireSoapBoxCli;
import com.soapboxrace.jaxb.xmpp.XMPP_EventTimedOutType;
import com.soapboxrace.jaxb.xmpp.XMPP_ResponseTypeEventTimedOut;
import io.quarkus.logging.Log;
import org.quartz.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Date;

@ApplicationScoped
public class DNFTimerBO {
    @Inject
    OpenFireSoapBoxCli openFireSoapBoxCli;

    @Inject
    Scheduler scheduler;

    public void scheduleDNF(EventSessionEntity eventSessionEntity, Long personaId) {
        JobDetail jobDetail = JobBuilder.newJob(TimerJob.class)
                .usingJobData("eventSessionId", eventSessionEntity.getId())
                .usingJobData("personaId", personaId)
                .build();
        Date triggerDate = DateBuilder.futureDate(eventSessionEntity.getEvent().getDnfTimerTime(), DateBuilder.IntervalUnit.MILLISECOND);
        Trigger trigger = TriggerBuilder.newTrigger()
                .startAt(triggerDate)
                .build();
        try {
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException ex) {
            Log.error("Failed to schedule job", ex);
        }
    }

    public void sendDNFToDriver(long eventSessionId, long personaId) {
        XMPP_EventTimedOutType eventTimedOut = new XMPP_EventTimedOutType();
        eventTimedOut.setEventSessionId(eventSessionId);
        XMPP_ResponseTypeEventTimedOut eventTimedOutResponse = new XMPP_ResponseTypeEventTimedOut();
        eventTimedOutResponse.setEventTimedOut(eventTimedOut);
        openFireSoapBoxCli.send(eventTimedOutResponse, personaId);
    }

    public static class TimerJob implements Job {
        @Inject
        DNFTimerBO dnfTimerBO;

        @Override
        public void execute(JobExecutionContext jobExecutionContext) {
            JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
            dnfTimerBO.sendDNFToDriver(dataMap.getLong("eventSessionId"), dataMap.getLong("personaId"));
        }
    }
}
