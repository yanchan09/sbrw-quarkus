/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.bo;

import com.soapboxrace.core.bo.util.AchievementCommerceContext;
import com.soapboxrace.core.bo.util.OwnedCarConverter;
import com.soapboxrace.core.dao.BasketDefinitionDAO;
import com.soapboxrace.core.dao.CarClassesDAO;
import com.soapboxrace.core.dao.CarDAO;
import com.soapboxrace.core.dao.PersonaDAO;
import com.soapboxrace.core.engine.EngineException;
import com.soapboxrace.core.engine.EngineExceptionCode;
import com.soapboxrace.core.events.CarAddedToDriver;
import com.soapboxrace.core.jpa.*;
import com.soapboxrace.jaxb.http.OwnedCarTrans;
import com.soapboxrace.jaxb.util.JAXBUtility;
import io.quarkus.scheduler.Scheduled;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@ApplicationScoped
public class CarSlotBO {

    @Inject
    CarDAO carDAO;

    @Inject
    PerformanceBO performanceBO;

    @Inject
    PersonaDAO personaDAO;

    @Inject
    DriverPersonaBO driverPersonaBO;

    @Inject
    AchievementBO achievementBO;

    @Inject
    TokenSessionBO tokenSessionBO;

    @Inject
    CarClassesDAO carClassesDAO;

    @Inject
    BasketDefinitionDAO basketDefinitionDAO;

    @Inject
    Event<CarAddedToDriver> carAddedToDriverEvent;

    @Inject
    Logger logger;

    @Scheduled(cron = "0 * * * * ?")
    @Transactional
    public void scheduledRemoval() {
        int numRemoved = carDAO.deleteAllExpired();
        if (numRemoved > 0) {
            logger.info("Removed {} expired cars", numRemoved);
        }
    }

    public List<CarEntity> getPersonasCar(Long personaId) {
        List<CarEntity> ownedCarEntities = carDAO.findByPersonaId(personaId);

        for (CarEntity carEntity : ownedCarEntities) {
            carEntity.getPaints().size();
            carEntity.getPerformanceParts().size();
            carEntity.getSkillModParts().size();
            carEntity.getVinyls().size();
            carEntity.getVisualParts().size();

            if (carEntity.getCarClassHash() == 0) {
                // CarClassHash can be set to 0 to recalculate rating/class
                performanceBO.calcNewCarClass(carEntity);
                carDAO.update(carEntity);
            }
        }

        return ownedCarEntities;
    }

    public int countPersonasCar(Long personaId) {
        return carDAO.findNumByPersonaId(personaId);
    }

    public CarEntity addCar(ProductEntity productEntity, PersonaEntity personaEntity) {
        Objects.requireNonNull(productEntity, "productEntity is null");

        /*
        rentals cannot be purchased unless you have one other car
        rentals will not be considered when evaluating whether or not you can sell a car
        so if you own one regular car and one rental, you cannot sell the regular car no matter what
        until you get another regular car
         */

        boolean isRental = productEntity.getDurationMinute() > 0;
        if (isRental) {
            Long numNonRentals = carDAO.findNumNonRentalsByPersonaId(personaEntity.getPersonaId());

            if (numNonRentals.equals(0L)) {
                throw new EngineException("Persona " + personaEntity.getName() + " has no non-rental cars", EngineExceptionCode.MissingRequiredEntitlements, true);
            }
        }

        OwnedCarTrans ownedCarTrans = getCar(productEntity);
        ownedCarTrans.setId(0L);
        ownedCarTrans.getCustomCar().setId(0);

        CarEntity carEntity = new CarEntity();
        carEntity.setPersona(personaEntity);
        OwnedCarConverter.trans2Entity(ownedCarTrans, carEntity);
        OwnedCarConverter.details2NewEntity(ownedCarTrans, carEntity);

        if (isRental) {
            carEntity.setExpirationDate(LocalDateTime.now().plusMinutes(productEntity.getDurationMinute()));
            carEntity.setOwnershipType("RentalCar");
        }

        carDAO.insert(carEntity);
        performanceBO.calcNewCarClass(carEntity);

        carAddedToDriverEvent.fire(new CarAddedToDriver(personaEntity, carEntity));

        CarClassesEntity carClassesEntity =
                carClassesDAO.find(carEntity.getName());

        AchievementTransaction transaction = achievementBO.createTransaction(personaEntity.getPersonaId());

        if (carClassesEntity != null) {
            AchievementCommerceContext commerceContext = new AchievementCommerceContext(AchievementCommerceContext.CommerceType.CAR_PURCHASE);
            commerceContext.setCarClassesEntity(carClassesEntity);
            commerceContext.setProductEntity(productEntity);
            transaction.add("COMMERCE", Map.of("persona", personaEntity, "ownedCar", carEntity, "commerceCtx", commerceContext));
            achievementBO.commitTransaction(personaEntity, transaction);
        }

        return carEntity;
    }

    public boolean sellCar(TokenSessionEntity tokenSessionEntity, Long personaId, Long serialNumber) {
        this.tokenSessionBO.verifyPersonaOwnership(tokenSessionEntity, personaId);

        CarEntity carEntity = carDAO.find(serialNumber);
        if (carEntity == null) {
            return false;
        }

        if ("RentalCar".equalsIgnoreCase(carEntity.getOwnershipType())) {
            return false;
        }

        PersonaEntity personaEntity = personaDAO.find(personaId);

        if (!removeCar(personaEntity, serialNumber)) {
            return false;
        }

        double cashTotal = personaEntity.getCash() + carEntity.getResalePrice();
        driverPersonaBO.updateCash(personaEntity, cashTotal);

        return true;
    }

    public boolean removeCar(PersonaEntity personaEntity, Long serialNumber) {
        CarEntity carEntity = carDAO.find(serialNumber);
        if (carEntity == null) {
            return false;
        }
        if (!carEntity.getPersona().getPersonaId().equals(personaEntity.getPersonaId())) {
            throw new EngineException(EngineExceptionCode.CarNotOwnedByDriver, false);
        }
        Long nonRentalCarCount = carDAO.findNumNonRentalsByPersonaId(personaEntity.getPersonaId());

        // If the car is not a rental, check the number of non-rentals
        if (!"RentalCar".equalsIgnoreCase(carEntity.getOwnershipType())) {
            if (nonRentalCarCount <= 1) {
                return false;
            }
        } else if (nonRentalCarCount == 0) {
            return false;
        }

        carDAO.delete(carEntity);

        int curCarIndex = personaEntity.getCurCarIndex();

        if (curCarIndex > 0) {
            // Best case: we just decrement the current car index
            personaEntity.setCurCarIndex(curCarIndex - 1);
        } else {
            // Worst case: count cars again and subtract 1 to get new index
            personaEntity.setCurCarIndex(carDAO.findNumByPersonaId(personaEntity.getPersonaId()) - 1);
        }

        personaDAO.update(personaEntity);

        return true;
    }

    private OwnedCarTrans getCar(ProductEntity productEntity) {
        Objects.requireNonNull(productEntity, "productEntity is null");
        String productId = productEntity.getProductId();
        BasketDefinitionEntity basketDefinitionEntity = basketDefinitionDAO.find(productId);
        if (basketDefinitionEntity == null) {
            throw new IllegalArgumentException(String.format("No basket definition for %s", productId));
        }
        String ownedCarTrans = basketDefinitionEntity.getOwnedCarTrans();
        OwnedCarTrans ownedCarTrans1 = JAXBUtility.unMarshal(ownedCarTrans, OwnedCarTrans.class);

        if (productEntity.getDurationMinute() != 0) {
            ownedCarTrans1.setOwnershipType("RentalCar");
        }

        // do this automatically in case we get any weird data in our table
        ownedCarTrans1.setHeat(1.0f);
        ownedCarTrans1.setDurability(100);
        ownedCarTrans1.getCustomCar().setResalePrice(productEntity.getResalePrice());

        return ownedCarTrans1;
    }
}
