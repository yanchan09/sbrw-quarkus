/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.xmpp;

import com.soapboxrace.core.xmpp.openfire.OpenfireXmppProvider;
import com.soapboxrace.core.xmpp.sbrwxmpp.SbrwXmppProvider;
import io.quarkus.runtime.Startup;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Typed;
import javax.inject.Inject;

import java.util.List;

@Startup
@ApplicationScoped
public class OpenFireRestApiCli {
    private XmppProvider provider;

    @Inject
    OpenfireXmppProvider openfireProvider;

    @Inject
    SbrwXmppProvider sbrwProvider;

    @PostConstruct
    public void init() {
        if (openfireProvider.isEnabled()) {
            provider = openfireProvider;
        } else if (sbrwProvider.isEnabled()) {
            provider = sbrwProvider;
        } else {
            throw new RuntimeException("No XMPP provider is enabled");
        }
    }


    public void createUpdatePersona(Long personaId, String password) {
        provider.createPersona(personaId, password);
    }

    public int getTotalOnlineUsers() {
        return provider.getOnlineUserCount();
    }

    public List<Long> getAllPersonaByGroup(Long personaId) {
        return provider.getAllPersonasInGroup(personaId);
    }

    public void sendChatAnnouncement(String message) {
        provider.sendChatAnnouncement(message);
    }

    public void sendMessage(long recipient, String message) {
        provider.sendMessage(recipient, message);
    }
}