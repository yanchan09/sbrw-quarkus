/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2020.
 */

package com.soapboxrace.core.xmpp;

import com.soapboxrace.jaxb.util.JAXBUtility;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class OpenFireSoapBoxCli {

    @Inject
    OpenFireRestApiCli restApi;

    public void send(String msg, Long to) {
        restApi.sendMessage(to, msg);
    }

    public void send(Object object, Long to) {
        restApi.sendMessage(to, JAXBUtility.marshal(object));
    }
}