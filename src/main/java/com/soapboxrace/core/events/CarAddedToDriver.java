/*
 * This file is part of the Soapbox Race World core source code.
 * If you use any of this code for third-party purposes, please provide attribution.
 * Copyright (c) 2022.
 */

package com.soapboxrace.core.events;

import com.soapboxrace.core.jpa.CarEntity;
import com.soapboxrace.core.jpa.PersonaEntity;
import lombok.Data;

@Data
public class CarAddedToDriver {
    private final PersonaEntity personaEntity;
    private final CarEntity carEntity;
}
