package com.soapboxrace.core.events;

import com.soapboxrace.core.jpa.PersonaEntity;
import lombok.Data;

@Data
public class PersonaCreated {
    private final PersonaEntity personaEntity;
}
