@XmlSchema(
        namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays",
        elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns = {
                @XmlNs(prefix = "array", namespaceURI = "http://schemas.microsoft.com/2003/10/Serialization/Arrays"),
        }
)
package com.soapboxrace.jaxb.http.arrays;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;