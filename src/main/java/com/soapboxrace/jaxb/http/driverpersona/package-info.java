@XmlSchema(
        namespace = "http://schemas.datacontract.org/2004/07/Victory.TransferObjects.DriverPersona",
        elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns = {
                @XmlNs(prefix = "", namespaceURI = "http://schemas.datacontract.org/2004/07/Victory.TransferObjects.DriverPersona"),
        }
)
package com.soapboxrace.jaxb.http.driverpersona;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;